package ca.tfuller.rotaryencoder;

import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.RaspiPinNumberingScheme;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;

/*
 * j733 rotary encoder
 * vcc: red
 * gnd: black
 * A: white
 * B: green
 * 
 * put 1k pull up resistors on A and B
 */
public class RotaryEncoder {
	protected static final Logger logger = LogManager.getLogger(RotaryEncoder.class);
	int counter = 0;

	GpioPinDigitalOutput pinLed;
	GpioPinDigitalInput pinButton;
	GpioPinDigitalInput pinChannelA;
	GpioPinDigitalInput pinChannelB;
	GpioController gpio;
	boolean recordingState = false;
	String direction;

	PinState lastState;

	public void setup() {
		// create gpio controller
		GpioFactory.setDefaultProvider(new RaspiGpioProvider(RaspiPinNumberingScheme.BROADCOM_PIN_NUMBERING));
		gpio = GpioFactory.getInstance();

		// provision gpio pin #01 as an output pin and turn on
		pinLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, "MyLED", PinState.HIGH);
		pinButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_24, "MyButton");
		pinChannelA = gpio.provisionDigitalInputPin(RaspiPin.GPIO_27, "ChannelA", PinPullResistance.PULL_UP);
		pinChannelB = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "ChannelB", PinPullResistance.PULL_UP);

		// set shutdown state for this pin
		pinLed.setShutdownOptions(true, PinState.LOW);

		initButtonInterrupt();
		initEncoderInterrupt();

	}

	public static void main(String[] args) {
		System.out.println("hello");
		logger.info("logger is working");

		RotaryEncoder rotaryEncoder = new RotaryEncoder();
		rotaryEncoder.setup();

		System.out.println("<--Pi4J--> GPIO Control Example ... started.");

		long startTime = System.currentTimeMillis();
		long endTime;
		long timeout = 300000;
		do {
			endTime = System.currentTimeMillis();
		} while (endTime - startTime < timeout);

		rotaryEncoder.shutdown();
		System.out.println("Exiting ControlGpioExample");
	}

	private void shutdown() {
		gpio.shutdown();
	}

	public void initButtonInterrupt() {
		pinButton.addTrigger(new GpioCallbackTrigger(new Callable<Void>() {
			public Void call() throws Exception {
				if (pinButton.isHigh()) {
					System.out.println(" --> GPIO TRIGGER CALLBACK RECEIVED ");
					recordingState = !recordingState;
					if (recordingState) {
						pinLed.high();
					} else {
						pinLed.low();
					}
				}
				return null;
			}
		}));
	}

	public void initEncoderInterrupt() {
		GpioCallbackTrigger channelTrigger = new GpioCallbackTrigger(new Callable<Void>() {
			public Void call() throws Exception {
				// Read the current state of CLK
				PinState currentStateChannelA = pinChannelA.getState();

				// If last and current state of CLK are different, then pulse occurred
				// React to only 1 state change to avoid double count
				if (currentStateChannelA != lastState && currentStateChannelA.isHigh()) {

					// If the DT state is different than the CLK state then
					// the encoder is rotating CCW so decrement
					if (!pinChannelB.getState().equals(currentStateChannelA)) {
						counter--;
						direction = "CCW";
					} else {
						// Encoder is rotating CW so increment
						counter++;
						direction = "CW";
					}

					logger.info("Position: " + counter + " direction " + direction);
				}

				// Remember last CLK state
				lastState = currentStateChannelA;

				return null;
			}
		});

		pinChannelA.addTrigger(channelTrigger);
		pinChannelB.addTrigger(channelTrigger);
	}
}
